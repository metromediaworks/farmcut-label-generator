
/**
 * First we will load all of this project's JavaScript dependencies which
 * includes Vue and other libraries. It is a great starting point when
 * building robust, powerful web applications using Vue and Laravel.
 */

require('./bootstrap');
window.Vue = require('vue');

import Buefy from 'buefy'
import vSelect from 'vue-select';


import moment from "moment";
import VueMomentJS from "vue-momentjs";

Vue.use(Buefy);
Vue.use(VueMomentJS, moment);
Vue.component('pagination', require('./components/pagination.js'));
Vue.component('shipment-component', require('./components/Shipment.vue'));
Vue.component('shipment-list-component', require('./components/ShipmentList.vue'));
Vue.component('product-list-component', require('./components/ProductList.vue'));
Vue.component('product-component', require('./components/Product.vue'));
Vue.component('v-select', vSelect);

/**
 * Next, we will create a fresh Vue application instance and attach it to
 * the page. Then, you may begin adding components to this application
 * or customize the JavaScript scaffolding to fit your unique needs.
 */

/**
 * Require JS
 */

document.addEventListener('DOMContentLoaded', function () {

    // Get all "navbar-burger" elements
    var $navbarBurgers = Array.prototype.slice.call(document.querySelectorAll('.navbar-burger'), 0);

    // Check if there are any nav burgers
    if ($navbarBurgers.length > 0 ) {

        // Add a click event on each of them
        $navbarBurgers.forEach(function ($el) {
            $el.addEventListener('click', function () {

                // Get the target from the "data-target" attribute
                var target = $el.dataset.target;
                var $target = document.getElementById(target);

                // Toggle the class on both the "navbar-burger" and the "navbar-menu"
                $el.classList.toggle('is-active');
                $target.classList.toggle('is-active');

            });
        });
    }

});