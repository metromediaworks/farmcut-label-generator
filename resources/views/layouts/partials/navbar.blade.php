
<nav class="navbar">
    <div class="navbar-brand">

        <p class="level">
                <a class="level-item has-text-centered link is-info" href="{{ url('/home') }}"><img src="/img/famcut-horizontal_-No-Tagline_TM.png" alt=""></a>
        </p>

        <div class="navbar-burger burger " data-target="navMenubd-example">
            <span></span>
            <span></span>
            <span></span>
        </div>
    </div>

    <div id="navMenubd-example" class="navbar-menu">
        {{--<div class="navbar-start">--}}
            {{--<a class="navbar-item " href="{{ url('/label/generate') }}">--}}
                {{--Labels &ensp; <i class="fa fa-barcode"></i>--}}
            {{--</a>--}}
            {{--<a class="navbar-item " href="{{ url('/shipment') }}">--}}
                {{--Shipment &ensp; <i class="fa fa-truck"></i>--}}
            {{--</a>--}}
        {{--</div>--}}

        <div class="navbar-end">
            <div class="navbar-item has-dropdown is-hoverable">
                <a class="navbar-link is-active">
                    <span class="icon">
                        <i class="fa fa-user"></i>
                    </span>
                    <span>
                        {{ $authUser->name  }}
                    </span>
                </a>
                <div class="navbar-dropdown ">
                    <a class="navbar-item " href="{{ route('logout')  }}">
                        <span>
                            <i class="fa fa-unlock fa-lg"></i> &ensp; Logout
                        </span>
                    </a>
                </div>
            </div>
        </div>
    </div>
</nav>