<!doctype html>
<html lang="{{ app()->getLocale() }}">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="csrf-token" content="{{csrf_token()}}">

    <title>FarmCut Login</title>

    <!-- Fonts -->
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">
    <link rel="stylesheet" href="{{asset('css/app.css')}}">
    @stack('styles')
</head>
<body>




    <div id="app">
        <div class="auth-pages">
            <section class="hero">
                <div class="hero-body">
                    <div class="container">
                        @yield('content')
                    </div>
                </div>
            </section>
        </div>

    </div>

    <script src="{{asset('js/app.js')}}"></script>
    @stack('scripts')

</body>
</html>
