<!doctype html>
<html lang="{{ app()->getLocale() }}">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="csrf-token" content="{{csrf_token()}}">

    <title>FarmCut</title>

    <!-- Fonts -->
    @if ( isset($pdf) )
        <link rel="stylesheet" href="css/buefy.min.css" />
        <link rel="stylesheet" href="css/pdf.css" />

    @else
        <link rel="stylesheet" href="/css/buefy.min.css" />
        <link rel="stylesheet" href="/css/pdf.css" />

    @endif
</head>
<body>

    <div id="app">

        <section class="main-content columns is-fullheight">

            <div class="container column is-10">
                @yield('content')
            </div>

        </section>
    </div>

</body>
</html>
