<!doctype html>
<html lang="{{ app()->getLocale() }}">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="csrf-token" content="{{csrf_token()}}">

    <title>FarmCut</title>

    <!-- Fonts -->
    {{--@if ( !isset($pdf) )--}}
        <link rel="stylesheet" href="{{mix('css/app.css')}}">
    {{--@else--}}
        {{--<link rel="stylesheet" href="/css/pdf.css" media="print" />--}}
    {{--@endif--}}
    {{--<link rel="stylesheet" href="{{asset('css/vue-multiselect/vue-multiselect.min.css')}}">--}}

</head>
<body>

    <div id="app">
            @include('layouts.partials.navbar')

        <section class="main-content is-fullheight">
            
            <div class="columns is-gapless">
                <aside class="menu column is-2 is-narrow-mobile is-fullheight section is-hidden-mobile">
                    @include('layouts.partials.sidebar')
                </aside>
                <div class="column is-10">
                    <div class="content-box">
                        <div class="container is-fluid">
                            @yield('content')
                        </div>
                    </div>
                </div>
            </div>

        </section>
    </div>

    <script src="{{mix('js/main.js')}}"></script>
    @stack('scripts')

</body>
</html>
