@extends('layouts.master')

@section('content')

    <shipment-list-component></shipment-list-component>

@endsection

@push('scripts')
<script>

    $(document).ready(function() {
        $('.sidebar-item').removeClass('is-active');
        $('#sidebar-shipment').addClass('is-active');
    });

</script>
@endpush