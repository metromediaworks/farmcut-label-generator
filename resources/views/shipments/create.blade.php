@extends('layouts.master')

@section('content')
    <shipment-component shipment-raw="{{$shipment}}"></shipment-component>
@endsection

@push('scripts')
<script>

    $(document).ready(function() {
        $('.sidebar-item').removeClass('is-active');
        $('#sidebar-shipment').addClass('is-active');
    });

</script>
@endpush