@extends('layouts.master')

@section('content')

    <div class="shipment-print" >

        @if ( !isset($pdf) )
            <div class="screen-only">
                <button onclick="window.print();" class="print button is-primary">Print</button>
            </div>
        @endif

        <div class="columns" >
            <div class="navbar-brand">
                @if ( isset($pdf) )
                <div class="app-logo top-spacer">
                    <img src="/img/TH-FUEL.png" />
                </div>
                @else
                <div class="app-logo top-spacer print-only">
                    <img src="/img/TH-FUEL.png" />
                </div>
                @endif
            </div>
        </div>

        <div class="columns">

            <div class="column address-from-pre">
                From:
            </div>

            <div class="is-one-third column address-from">
                Farm Cut<br>
                180 Maiden Lane<br>
                New York, NY 10038<br>
            </div>
            <div class="column address-to-pre">
                To:
            </div>
            <div class="is-one-third column address-to">
                {{$currentShipment->location->name}}<br>
                {{$currentShipment->location->address}}<br>
                @if ( !empty($currentShipment->location->address2 ) )
                {{$currentShipment->location->address2}}<br>
                @endif
                {{$currentShipment->location->city}}, {{$currentShipment->location->state}} {{$currentShipment->location->zip}}
            </div>
        </div>

        <div class="columns">
            <div class="column">
                <table class="shipment-header table is-bordered is-fullwidth">
                    <thead>
                    <tr>
                        <th>Order Number</th>
                        <th>Date</th>
                        <th>Scan-In</th>
                    </tr>
                    </thead>
                    <tbody>
                    <tr>
                        <td>{{$currentShipment->sku}}</td>
                        <td>{{$currentShipment->created_at}}</td>
                        <td>
                        <img src="data:image/png;base64,{!! DNS1D::getBarcodePNG($currentShipment->sku,"C39",1,45,array(0,0,0),1)!!}" alt="barcode" />
                        <div class="sku-code">{{$currentShipment->sku}}</div>
                        </td>
                        </td>
                    </tr>
                    </tbody>
                </table>
            </div>
        </div>

        <div class="columns">

            <div class="column">
                <table class="shipment-details table is-bordered is-fullwidth">
                    <thead>
                    <tr>
                        <th>Qty</th>
                        <th>Item Code</th>
                        <th>Description</th>
                    </tr>
                    </thead>
                    <tbody>
                    @foreach ($currentShipment->products as $product)
                    <tr>
                        <td>{{$product->pivot->quantity}}</td>
                        <td>{{$product->sku}}</td>
                        <td>{{$product->name}}</td>
                    </tr>
                    @endforeach
                    </tbody>
                    <tfoot>
                    </tfoot>
                </table>
            </div>
        </div>

    </div>


    </div>
@endsection

@push('scripts')
<script>

    $(document).ready(function() {
        $('.sidebar-item').removeClass('is-active');
        $('#sidebar-shipment').addClass('is-active');

        @if ( isset($print_now) )
        window.print();
        window.setTimeout(function() {
            document.location.href = "/shipment";
        }, 4000);

        @endif
    });

</script>
@endpush
