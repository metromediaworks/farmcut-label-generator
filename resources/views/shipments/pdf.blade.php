@extends('layouts.pdf')

@section('content')

    <div class="container shipment-print" >


        <div class="columns" >
            <div class="navbar-brand">
                <div class="app-logo top-spacer">
                    @if ( isset($pdf) )
                        <img src="img/TH-FUEL.png" />
                    @else
                        <img src="/img/TH-FUEL.png" />
                    @endif
                </div>
            </div>
        </div>

        <table class="address">
            <tr>
                <td class="address-from-pre">From:</td>
                <td class="address-from">
                    Farm Cut<br>
                    180 Maiden Lane<br>
                    New York, NY 10038<br>
                </td>
                <td class="address-to-pre">To:</td>
                <td class="address-to">
                    {{$currentShipment->location->name}}<br>
                    {{$currentShipment->location->address}}<br>
                    @if ( !empty($currentShipment->location->address2 ) )
                        {{$currentShipment->location->address2}}<br>
                    @endif
                    {{$currentShipment->location->city}}, {{$currentShipment->location->state}} {{$currentShipment->location->zip}}
                </td>
            </tr>
        </table>

        <div class="columns">
            <div class="column">
                <table class="shipment-header table is-bordered is-fullwidth">
                    <thead>
                    <tr>
                        <th>Order Number</th>
                        <th>Date</th>
                        <th>Scan-In</th>
                    </tr>
                    </thead>
                    <tbody>
                    <tr>
                        <td>{{$currentShipment->sku}}</td>
                        <td>{{$currentShipment->created_at}}</td>
                        <td>
                        {{--<img width="500" src="data:image/png;base64,{!! DNS1D::getBarcodePNGPath($currentShipment->sku,"C39",5,100,array(0,0,0),1)!!}" alt="barcode" />--}}
                        {!! DNS1D::getBarcodeHTML($currentShipment->sku, "C39" ,1,45) !!}
                        <div class="sku-code">{{$currentShipment->sku}}</div>
                        </td>
                    </tr>
                    </tbody>
                </table>
            </div>
        </div>

        <div class="columns">

            <div class="column">
                <table class="shipment-details table is-bordered is-fullwidth">
                    <thead>
                    <tr>
                        <th>Qty</th>
                        <th>Item Code</th>
                        <th>Description</th>
                    </tr>
                    </thead>
                    <tbody>
                    @foreach ($currentShipment->products as $product)
                    <tr>
                        <td>{{$product->pivot->quantity}}</td>
                        <td>{{$product->sku}}</td>
                        <td>{{$product->name}}</td>
                    </tr>
                    @endforeach
                    </tbody>
                    <tfoot>
                    </tfoot>
                </table>
            </div>
        </div>

    </div>

    </div>
@endsection
