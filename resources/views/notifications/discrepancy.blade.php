@component('mail::layout')

    {{-- Header --}}
    @slot('header')
        @component('mail::header', ['url' => config('app.url')])
            <img src="{{url('/img/farmcut_white-text-TM-logo-and-slogan.png')}}" alt="farm cut, pure food, pure comfort">
        @endcomponent
    @endslot

Hello! <br><br>
There was a difference reported between the amount received and the amount shipped on Order #{{$discrepancies->first()->shipment_id}} received by {{$discrepancies->first()->user_email}}.

| Product Id  |    Product Name    |  Shipped  |  Received  |
|:-----------: |:----------------: |:--------: |:---------: |
@foreach($discrepancies as $discrepancy)
| {{$discrepancy->product->id}}  | {{$discrepancy->product->name}}  | {{$discrepancy->shipped}}   | {{$discrepancy->received}}  |
@endforeach
<br>
Thanks, <br>
Admin

    @slot('footer')
        @component('mail::footer')
            © {{ date('Y') }} {{ config('app.name') }}. All rights reserved.
        @endcomponent
    @endslot
@endcomponent