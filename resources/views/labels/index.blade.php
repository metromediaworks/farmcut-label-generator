@extends('layouts.master')

@section('content')
    <div class="container">
        <div class="content">
            <h1>Generate label</h1>
        </div>
        <div>
            <div class="existent-product-container">
                <div class="has-text-centered columns existent-product-title">
                    <h2>Generate Label from Existent Product</h2>
                </div>
                <div class="columns is-mobile is-centered existent-product-content">
                    <div class="column is-half is-narrow">
                        {!! Form::open(['url' => 'label/generate/']) !!}
                            <div class="field">
                                <div class="control has-icons-left">
                                    <span class="select is-fullwidth">
                                      {!! Form::select('product_id', $products, null, ['placeholder' => 'Select Product']) !!}
                                    </span>
                                    <span class="icon is-small is-left">
                                      <i class="fa fa-globe"></i>
                                    </span>
                                </div>
                                {!! $errors->first('product_id', '<span class="help is-danger">:message</p>') !!}
                            </div>
                            <div class="field">
                                <div class="control has-text-centered">
                                    <button type="submit" class="button is-primary">Continue</button>
                                </div>
                            </div>
                        {!! Form::close() !!}
                    </div>
                </div>
            </div>
            <div class="is-separator">
                <span>OR</span>
            </div>
            <div>
                <div class="has-text-centered">
                    <button class="button is-info" @click="continue_with_new_product = !continue_with_new_product">
                        Generate Label from new Product
                    </button>
                </div>
                <transition class="new-product-form" name="slide-fade">
                    <div v-if="continue_with_new_product || {{ old('new_product') ? 1 : 0  }}" v-cloak>
                        {!! Form::open(['url' => 'label/generate/']) !!}

                        {!! Form::hidden('new_product', true) !!}

                        <div class="field">
                            <label for="name" class="label">Name</label>
                            <div class="control">
                                {!! Form::text('name', null, ['class' => 'input']) !!}
                            </div>
                            {!! $errors->first('name', '<span class="help is-danger">:message</p>') !!}
                        </div>
                        <div class="columns">
                            <div class="column">
                                <div class="field">
                                    <label for="sku" class="label">SKU</label>
                                    <div class="control">
                                        {!! Form::text('sku', null, ['class' => 'input']) !!}
                                    </div>
                                    {!! $errors->first('sku', '<span class="help is-danger">:message</p>') !!}
                                </div>
                            </div>
                            <div class="column">
                                <div class="field">
                                    <label for="price" class="label">Price</label>
                                    <div class="control">
                                        {!! Form::text('price', null, ['placeholder' => '$00.00', 'class' => 'input']) !!}
                                    </div>
                                    {!! $errors->first('price', '<span class="help is-danger">:message</p>') !!}
                                </div>
                            </div>
                        </div>
                        <div class="columns">
                            <div class="field column">
                                <label for="ship_date" class="label">Ship Date</label>
                                <div class="control">
                                    {!! Form::date('ship_date', null, ['class' => 'input']) !!}
                                </div>
                                {!! $errors->first('ship_date', '<span class="help is-danger">:message</p>') !!}
                            </div>
                            <div class="field column">
                                <label for="sell_by" class="label">Sell By</label>
                                <div class="control">
                                    {!! Form::date('sell_by', null, ['class' => 'input']) !!}
                                </div>
                                {!! $errors->first('sell_by', '<span class="help is-danger">:message</p>') !!}
                            </div>
                        </div>
                        <div class="columns">
                            <div class="is-12">
                                <b-field label="Name" v-for="key_statement in key_statements">
                                    <b-input value="" :name="'key_statement'+key_statement"></b-input>
                                </b-field>
                            </div>
                        </div>
                        <div class="field">
                            <div class="control">
                                <b-field label="Description">
                                    <b-input maxlength="100" type="textarea" placeholder="Description" name="description" rows="2"></b-input>
                                </b-field>
                            </div>
                            {!! $errors->first('description', '<span class="help is-danger">:message</p>') !!}
                        </div>
                        <div class="field">
                            <div class="control">
                                <b-field label="Ingredients">
                                    <b-input maxlength="100" type="textarea" placeholder="Ingredients" name="ingredients" rows="2"></b-input>
                                </b-field>
                            </div>
                            {!! $errors->first('ingredients', '<span class="help is-danger">:message</p>') !!}
                        </div>
                        <div class="field">
                            <div class="control">
                                <b-field label="Special Instructions">
                                    <b-input maxlength="100" type="textarea" placeholder="Special Instructions" name="special_instructions" rows="2"></b-input>
                                </b-field>
                            </div>
                            {!! $errors->first('special_instructions', '<span class="help is-danger">:message</p>') !!}
                        </div>

                        <div class="field">
                            <div class="control has-text-centered">
                                <button type="submit" class="button is-primary">Continue</button>
                            </div>
                        </div>
                        {!! Form::close() !!}
                    </div>
                </transition>
            </div>
        </div>
    </div>
@endsection