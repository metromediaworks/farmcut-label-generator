@extends('layouts.master')

@section('content')
    <div class="container">
        @if (Auth::user())
        <h1 class="title  has-text-centered">
            Quick Start
{{--            Welcome {{Auth::user()->name}}--}}
        </h1>
        @endif

        <section class="actions  has-text-centered">
                <a href="{{ url('/product/create') }}" class="button is-primary action">Add New Product</a> <br><br>
                <a href="{{ url('/shipment') }}" class="button is-primary action">Create New Shipment</a>
        </section>
    </div>
@endsection