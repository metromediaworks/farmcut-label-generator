@extends('layouts.master')

@section('content')
    
        <div class="content">
            <h1>Add Product</h1>
        </div>

        <product-component product-raw="{}" action="{{ route('product.store') }}"></product-component>


</div>
@endsection

@push('scripts')
<script>

    $(document).ready(function() {
        $('.sidebar-item').removeClass('is-active');
        $('#sidebar-product').addClass('is-active');
    });

</script>
@endpush