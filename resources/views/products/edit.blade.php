@extends('layouts.master')

@section('content')

        <div class="content">
            <h1>Edit Product</h1>
        </div>

        <product-component product-raw="{{$product}}" action="{{ route('product.update') }}"></product-component>

</div>
@endsection

@push('scripts')
<script>

    $(document).ready(function() {
        $('.sidebar-item').removeClass('is-active');
        $('#sidebar-product').addClass('is-active');
    });

</script>
@endpush