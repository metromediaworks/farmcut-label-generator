@extends('layouts.master')

@section('content')

    <product-list-component></product-list-component>

@endsection

@push('scripts')
<script>

    $(document).ready(function() {
        $('.sidebar-item').removeClass('is-active');
        $('#sidebar-product').addClass('is-active');
    });

</script>
@endpush