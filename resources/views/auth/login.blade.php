@extends('layouts.auth')

@section('content')
    <div class="columns is-mobile is-centered">
        <div class="column is-half is-narrow">
            <h1 class="login-title"><img src="/img/farmcut_white-text-TM-logo-and-slogan.png" alt=""></h1>
        </div>
    </div>
    <div class="columns is-mobile is-centered">
        <div class="column is-half is-narrow">
            <div class="card login-box">
                <header class="card-header">
                    <p class="card-header-title">
                        Login
                    </p>
                </header>


                <div class="card-content">
                    <div class="content">
                        {!! Form::open(['method' => 'POST']) !!}
                        <div class="field">
                            <div class="control has-icons-left has-icons-right">
                                {!! Form::email('email', null, ['placeholder' => 'Email', 'class' => 'input']) !!}
                                <span class="icon is-small is-left">
                                    <i class="fa fa-envelope"></i>
                                </span>
                                <span class="icon is-small is-right">
                                    <i class="fa fa-check"></i>
                                </span>
                            </div>
                            {!! $errors->first('email', '<p class="help is-danger">:message</p>') !!}
                        </div>
                        <div class="field">
                            <div class="control has-icons-left">
                                {!! Form::password('password', ['placeholder' => 'Password', 'class' => 'input']) !!}
                                <span class="icon is-small is-left">
                                    <i class="fa fa-lock"></i>
                                </span>
                            </div>
                            {!! $errors->first('password', '<p class="help is-danger">:message</p>') !!}
                        </div>
                        <div class="field">
                            <div class="control">
                                <label class="checkbox">
                                    {!! Form::checkbox('remember', null, null, ['style' => 'display: inline-block']) !!}
                                    <small>Remember me</small>
                                </label>
                            </div>
                        </div>
                        <div class="field is-grouped is-grouped-right">
                            <div class="control">
                                <button type="submit" class="button is-primary">
                                    Login
                                </button>
                            </div>
                            <a class="btn btn-link" href="{{ route('password.request') }}">
                                Forgot Your Password?
                            </a>
                        </div>
                        {!! Form::close() !!}
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection