let mix = require('laravel-mix');

/*
 |--------------------------------------------------------------------------
 | Mix Asset Management
 |--------------------------------------------------------------------------
 |
 | Mix provides a clean, fluent API for defining some Webpack build steps
 | for your Laravel application. By default, we are compiling the Sass
 | file for the application as well as bundling up all the JS files.
 |
 */

mix
    .copy('node_modules/vue-multiselect/dist/', 'public/css/vue-multiselect')
    .copy('resources/assets/img', 'public/img')
    .js('resources/assets/js/app.js', 'public/js')
    .js('resources/assets/js/index.js', 'public/js')
    .sass('resources/assets/sass/app.scss', 'public/css/temp')

    .combine(
        [
            'public/js/app.js',
            'public/js/index.js',
        ],
        'public/js/main.js'
    )

    .styles([
        'node_modules/buefy/lib/buefy.min.css',
        'node_modules/pnotify/dist/pnotify.css',
        'node_modules/sweetalert2/dist/sweetalert2.css',
        'public/css/temp/app.css',
    ], 'public/css/app.css')

    .styles([
        'node_modules/buefy/lib/buefy.min.css',
        'resources/assets/css/pdf.css',
    ], 'public/css/pdf.css')

    .version()
    .sourceMaps()
;
