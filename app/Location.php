<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use OwenIt\Auditing\Contracts\Auditable;

class Location extends Model implements Auditable
{
    use \OwenIt\Auditing\Auditable;

    protected $fillable = [
        'name',
        'address',
        'address2',
        'state',
        'city',
        'zip',
        'phone',
        'alt_phone',
        'brand_id',
    ];

    protected $appends = [
        'full_address',
    ];


    public function products()
    {
        return $this->belongsToMany('App\Product', 'location_product')->withPivot('par_quantity');
    }

    public function getFullAddressAttribute()
    {
        return sprintf(
            '%s %s, %s, %s %s',
            ucfirst($this->getAttribute('address')),
            ucfirst($this->getAttribute('address2')),
            ucfirst($this->getAttribute('city')),
            ucfirst($this->getAttribute('state')),
            $this->getAttribute('zip')
        );
    }
}
