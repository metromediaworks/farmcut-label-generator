<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use OwenIt\Auditing\Contracts\Auditable;

class Category extends Model implements Auditable
{
    use SoftDeletes, \OwenIt\Auditing\Auditable;

    protected $fillable = [
        'name',
        'parent_id'
    ];

    protected $dates = [
        'created_at',
        'updated_at',
        'deleted_at',
    ];


    public function products()
    {
        return $this->hasMany('App\Product');
    }

    public function subcategories()
    {
        return $this->hasMany('App\Category', 'parent_id', 'id');
    }

    public function parentCategory()
    {
        return $this->belongsTo('App\Category', 'parent_id');
    }

}
