<?php
/**
 * Created by PhpStorm.
 * User: danielcastro
 * Date: 2/2/17
 * Time: 2:06 PM
 */

use Illuminate\Support\Debug\Dumper;
use Illuminate\Support\Facades\Log;

if( !function_exists('float_to_pennies') ){
    /**
     * Take a float number an return the same value * 100. 100.00 -> 1000
     *
     * @param $float
     * @return int
     */
    function float_to_pennies( $float )
    {
        return intval(float_multiply($float));
    }
}

if( !function_exists('pennies_to_float') ){
    /**
     * Take an integer and converts it to float rounded to two digits 1000 -> 100.00
     *
     * @param $int
     * @return float
     */
    function pennies_to_float( $int )
    {
        return round(floatval($int / 100), 2);
    }
}

if( !function_exists('roundfloat') ){
    /**
     * Take a float number an return the casted number to float rounded to two digits
     *
     * @param $number
     * @param int $round
     * @return float
     */
    function roundfloat( $number, $round = 2 )
    {
        return round(floatval($number), $round);
    }
}

if( !function_exists('float_multiply') ){
    /**
     * Take a float number an return the casted number to float rounded with two digits
     *
     * @param $integer
     * @param int $_scale
     * @return float
     */
    function float_multiply( $integer, $_scale = 2 )
    {
        return round($integer * 100, $_scale);
    }
}

if( !function_exists('tax_rate') ){
    /**
     * Take a float number an return the casted number to float rounded to two digits
     *
     * @param $price
     * @param $tax
     * @param int $quantity
     * @return float
     */
    function tax_rate( $price, $tax, $quantity = 1 )
    {
        $taxRate = $tax / 100;
        $taxOperation = ( $price * $taxRate ) * $quantity;
        return roundfloat($taxOperation);
    }
}

if( !function_exists('d') ){
    /**
     * Dump the passed variables and end the script.
     *
     * @param  mixed
     * @return void
     */
    function d()
    {
        array_map(function ( $x ) {
            ( new Dumper )->dump($x);
        }, func_get_args());
    }
}

if( !function_exists('array_to_object') ){
    /**
     * Dump the passed variables and end the script.
     *
     * @param  array $array
     * @return object
     */
    function array_to_object($array)
    {
        return json_decode(json_encode($array));
    }
}

if ( !function_exists('trace') ) {

    function trace($msg, $die = false) {
        $s = print_r($msg, true);
        $s .= "\n";
        error_log($s, 3, storage_path("logs/trace.log"));
        if ( $die ) {
            echo ('<pre>');
            print_r($s);
            echo ('</pre>');
        }
    }
}