<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class GenerateLabelRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'product_id'           => 'required_without:new_product',
            'new_product'          => 'required_without:product_id',

            //New Product Information
            'name'                 => 'string',
            'sku'                  => '',
            'price'                => '',
            'ship_date'            => 'date',
            'sell_by'              => 'date',
            'description'          => '',
            'ingredients'          => '',
            'special_instructions' => '',
        ];
    }


    public function messages()
    {
        return [
            'product_id.required_without' => 'You must select a product or create a new one.',
            'new_product.required_without' => 'You must select a product or create a new one.',
        ];

    }
}
