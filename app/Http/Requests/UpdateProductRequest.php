<?php
/**
 * Created by PhpStorm.
 * User: andresabello
 * Date: 3/9/18
 * Time: 11:25 AM
 */

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class UpdateProductRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        $product = $this->request->all();

        return [
            "product" => 'required|array|min:3',
            'product.category_id' => 'required',
            'product.sku' => 'required|unique:products,id,'.$product['product']['id'],
            'product.name' => 'required|unique:products,id,'.$product['product']['id'],
        ];
    }

    public function messages()
    {
        return [
            'product.category_id.required' => 'You must provide both a Category and a Sub Category',
            'product.sku.unique' => 'Product must have a unique SKU',
            'product.sku.required' => 'Product must have a unique SKU',
            'product.name.required' => 'Product must have a unique Name',
            'product.name.unique' => 'Product must have a unique Name',
        ];
    }
}