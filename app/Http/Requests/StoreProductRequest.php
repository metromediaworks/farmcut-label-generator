<?php
/**
 * Created by PhpStorm.
 * User: andresabello
 * Date: 3/8/18
 * Time: 3:33 PM
 */

namespace App\Http\Requests;


use Illuminate\Foundation\Http\FormRequest;

class StoreProductRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            "product" => 'required|array|min:3',
            'product.category_id' => 'required',
            'product.sku' => 'required|unique:products,sku',
            'product.name' => 'required|unique:products,name',
        ];
    }

    public function messages()
    {
        return [
            'product.category_id.required' => 'You must provide both a Category and a Sub Category',
            'product.sku.unique' => 'Product must have a unique SKU',
            'product.sku.required' => 'Product must have a unique SKU',
            'product.name.required' => 'Product must have a unique Name',
            'product.name.unique' => 'Product must have a unique Name',
        ];
    }
}