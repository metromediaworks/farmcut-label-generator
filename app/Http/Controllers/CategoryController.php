<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Category;

class CategoryController extends Controller
{
    /* ********************************************************
    Ajax calls
    */
    public function tree(Request $request)
    {

        $data = $request->input();

        $categories = Category::whereNull('parent_id')->with('subcategories.products')->get();

        $groomed_categories = $this->getGroomedCategories($categories);

        return ['success' => 1, 'data' => $groomed_categories];
    }



    public function add(Request $request)
    {

        $data = $request->input();

        $category = Category::where('name', $data['name'])
            ->where('parent_id', $data['parent_id'])
            ->first();

        if  ( $category ) {
            $msg = "Sub Category '{$data['name']}' already exists";
            if ( empty($data['parent_id']) ) {
                $msg = "Main Category '{$data['name']}' already exists";
            }
            return ['success' => 0, 'msg' => $msg];
        }

        $category = Category::create($data);

        if (! $category ) {
            return ['success' => 0];
        }

        $categories = Category::whereNull('parent_id')->with('subcategories')->get();

        $groomed_categories = $this->getGroomedCategories($categories);
        return ['success' => 1, 'data' => $groomed_categories];

    }


    public function delete(Request $request)
    {

        $data = $request->input();

        $category = Category::find($data['id']);

        if ( $category ) {
            $category->delete();
        }

        $categories = Category::whereNull('parent_id')->with('subcategories')->get();

        $groomed_categories = $this->getGroomedCategories($categories);
        return ['success' => 1, 'data' => $groomed_categories];
    }



    private function getGroomedCategories($categories)
    {

        $groomed_categories = [];
        $groomed_subcategories = [];
        $c = [];
        $s = [];

        foreach ($categories as $category) {
            unset($groomed_subcategories);
            $groomed_subcategories = [];
            if (count($category->subcategories)) {
                foreach ($category->subcategories as $subcategory) {
                    unset($s);
                    $s = ['id' => $subcategory->id, 'name' => $subcategory->name, 'parent_id' => $subcategory->parent_id, 'has_children' => 0];
                    if (count($subcategory->products)) {
                        $s['has_children'] = 1;
                    }
                    $groomed_subcategories[] = $s;
                }
            }
            unset($c);
            $c = ['id' => $category->id, 'name' => $category->name, 'parent_id' => $category->parent_id, 'has_children' => 0, 'subcategories' => []];
            if (count($groomed_subcategories)) {
                $c['has_children'] = 1;
                $c['subcategories'] = $groomed_subcategories;
            }
            $groomed_categories[] = $c;

        }

        return ($groomed_categories);
    }

}
