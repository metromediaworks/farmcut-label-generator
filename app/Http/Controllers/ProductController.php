<?php

namespace App\Http\Controllers;

use App\Http\Requests\StoreProductRequest;
use App\Http\Requests\UpdateProductRequest;
use App\Product;
use Illuminate\Http\Request;
use Illuminate\Pagination\LengthAwarePaginator;
use Validator;

class ProductController extends Controller
{
    protected $validation_rules = [
        'sku' => 'required',
        'name' => 'required',
        'price' => 'required',
        'category_id' => 'required',
    ];


    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return view('products.index');
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('products.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param StoreProductRequest $request
     * @return mixed
     */
    public function store(StoreProductRequest $request)
    {
        $product = $request->input('product');
//        $name = strtolower($product['name']);
//        $newProduct = Product::whereRaw("LOWER(name) = '{$name}'")->first();

//        if ($newProduct) {
//            return response()->json(['errors' => [
//                'product.name' => [
//                    'Duplicate product, please change the name.'
//                ]
//            ]], 422);
//        }

        $newProduct = Product::create($product);

        if ($newProduct) {
            $sync = [];
            foreach ($product['locations'] as $location) {
                $sync[$location['id']] = ['par_quantity' => $location['pivot']['par_quantity']];
            }
            $newProduct->locations()->sync($sync);
        }


        return (['success' => 1]);
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Product $product
     * @return void
     */
    public function show(Product $product)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $product = Product::with('locations')->find($id);
        $product = json_encode($product);
        return view('products.edit', compact('product'));
    }


    /* ********************************************************
    Ajax calls
    */
    public function list(Request $request)
    {

        $data = $request->input();

//        dd($data);

        if (!empty($data['filter']['keyword'])) {
            $products = Product::where('sku', 'like', "{$data['filter']['keyword']}%")
                ->orWhere('name', 'like', "%{$data['filter']['keyword']}%")
                ->orderBy('name', 'asc')
                ->get()->toArray();

        } else {
            $products = Product::orderBy('name', 'asc')
                ->get()->toArray();
        }
//        trace($products);

        $perPage = $data['nbItems'];

        if ($perPage <= 0) {
            $perPage = count($products);
        }

        $currentPage = intval($request->input('page', 1));
        $offset = ($currentPage * $perPage) - $perPage;
        $currentPageSearchResults = array_slice($products, $offset, $perPage, true);
        $pagination = new LengthAwarePaginator($currentPageSearchResults, count($products), $perPage, $currentPage,
            ['path' => $request->url(), 'query' => $request->query()]);

        $view_data = array('pagination' => $pagination);
        //trace($view_data);
        return (json_encode($view_data));
    }


    /**
     * Remove the specified resource from storage.
     *
     * @param Request $request
     * @return array {success :1} or {success:0 , msg : error_msg} or {success:0 , msg : error_msg}
     * @throws \Exception
     */
    public function delete(Request $request)
    {
        $data = $request->input();
        if (!empty($data['id'])) {
            Product::find($data['id'])->delete();
        }
        return (['success' => 1]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param UpdateProductRequest $request
     * @return array
     * @throws \Illuminate\Validation\ValidationException
     */
    public function save(UpdateProductRequest $request)
    {
        $product = $request->get('product');

        if (!isset($product['id']) || $product['id'] <= 0) {
            $skus = Product::whereNotNull('sku')->distinct()->get(['sku']);
            $skus = $skus->pluck('sku')->all();
            $list = implode(',', $skus);
            $this->validation_rules['sku'] = 'required|' . 'not_in:' . $list;
        }

        Validator::make($product, $this->validation_rules, [
            'sku.not_in' => 'Cannot duplicate SKUs',
            'category_id' => 'You must provide both a Category and a Sub Category',
        ])->validate();

        $new_product = Product::updateOrCreate(['id' => $product['id']], $product);

        if ($new_product) {
            $sync = [];
            foreach ($product['locations'] as $location) {
                $sync[$location['id']] = ['par_quantity' => $location['pivot']['par_quantity']];
            }
            $new_product->locations()->sync($sync);
        }


        return (['success' => 1]);
    }

    /**
     * Get a distinct list of all instructions
     *
     */
    public function instructions(Request $request)
    {
        $instructions = Product::whereNotNull('special_instructions')->distinct()->get(['special_instructions']);
        return (['success' => 1, 'data' => $instructions]);

    }

    /**
     * Get a distinct list of all skus
     *
     */
    public function skus(Request $request)
    {
        $skus = Product::whereNotNull('sku')->distinct()->get(['sku']);
        return (['success' => 1, 'data' => $skus]);

    }

    /**
     * Get a distinct list of all names
     *
     */
    public function names(Request $request)
    {
        $names = Product::whereNotNull('name')->distinct()->get(['name']);
        return (['success' => 1, 'data' => $names]);
    }

}
