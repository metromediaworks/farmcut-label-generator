<?php

namespace App\Http\Controllers;

use App\Shipment;
use App\Location;
use App\Product;
use Illuminate\Http\Request;
use PDF;
use Illuminate\Pagination\Paginator;
use Illuminate\Pagination\LengthAwarePaginator;
use Validator;

use Mpdf\Mpdf;

class ShipmentController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return view('shipments.index');
    }


    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $shipment = '{}';
        return view('shipments.create', compact('shipment'));
        //return view('shipments.create');
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function duplicate(\App\Shipment $shipment)
    {
        if ( $shipment )  {
            $shipment->load('location');
            $shipment->load('products');
            $shipment = json_encode($shipment);
        }
        else {
            $shipment = '{}';
        }

        return view('shipments.create', compact('shipment'));
    }


    /**
     * Remove the specified resource from storage.
     * @param  \App\Shipment  $shipment
     * @return \Illuminate\Http\Response
     */
    public function destroy(Shipment $shipment)
    {

    }



    public function print($id) {

        if ( isset($id) ) {
            $currentShipment = Shipment::with('location', 'products')
                ->find($id);

            $print_now = 1;

            if ( $currentShipment ) {
                return view('shipments.show')
                    ->with(compact('currentShipment'))
                    ->with(compact('print_now'));
            }
        }
        return view('shipments.create');
    }


    public function show($id) {

        if ( isset($id) ) {
            $currentShipment = Shipment::with('location', 'products')
            ->find($id);

            if ( $currentShipment ) {
                return view('shipments.show')
                    ->with(compact('currentShipment'));
            }
        }
        return view('shipments.create');
    }


    public function download(Shipment $shipment) {
        $currentShipment = $shipment;

        PDF::setOptions([
            'dpi' => 72,
//            'defaultFont' => 'sans-serif',
//            'defaultMediaType' => 'print',
//            'defaultPaperSize'=> 'letter',
//            'isHtml5ParserEnabled' => true,
//            'isPhpEnabled' => true
        ]);

        $html = view('shipments.pdf')
            ->with(compact('currentShipment'))
            ->with('pdf', true)
            ->render();


//        $pdf = PDF::loadView('shipments.show', ['currentShipment' => $currentShipment, 'pdf' => true]);
//        //dd($pdf);
//        return $pdf->download("shipment{$shipment->id}.pdf");


//        return view('shipments.pdf')
//        ->with(compact('currentShipment'));

        $pdf = PDF::loadHTML($html);
        return $pdf->stream();

    }


    /**
     * Ajax calls
     */

    public function getLocations(Request $request)
    {
        $data = $request->input();
        $locations = Location::all();
        return ($locations);
    }

    public function getProducts(Request $request)
    {
        $data = $request->input();
        $products = Product::all();
        return ($products);
    }

    public function findMatchingShipments(Request $request)
    {
        $data = $request->input();
        $shipments = Shipment::where('sku', 'like', "%{$data['fragment']}%")->orderBy('sku')->get();

        return (['success' => 1, 'data' => $shipments]);
    }


    public function save(Request $request)
    {

        $data = $request->input();
        $data = $data['shipment'];

        $shipment = Shipment::create(['sku' => '0', 'location_id' => $data['location']['id']]);

        if ( !$shipment ) {
            return(['success' => 0, 'msg' => 'Unable to create shipment']);
        }
        $shipment->sku = sprintf("%08d", $shipment->id);
        $shipment->save();

        $product_ids = array_column($data['products'], 'id');
        $shipment->products()->sync($product_ids);

        foreach($data['products'] as $product ) {
            $shipment->products()->updateExistingPivot($product['id'], ['quantity' => $product['pivot']['quantity']]);
        }

        $shipment->load('location', 'products');

        return(['success' => 1, 'data' => $shipment]);
    }


    public function list(Request $request) {

        $data = $request->input();

        if ($data['sortOrder']) {
            $products = Shipment::all()->sortBy($data['sortKey'])->values()->all();
        } else {
            $products = Shipment::all()->sortByDesc($data['sortKey'])->values()->all();
        }

        $perPage = $data['nbItems'];

        if ( $perPage <= 0 ) {
            $perPage = count($products);
        }

        $currentPage = intval($request->input('page', 1));
        $offset = ($currentPage * $perPage) - $perPage;
        $currentPageSearchResults = array_slice($products, $offset, $perPage, true);
        $pagination = new LengthAwarePaginator($currentPageSearchResults, count($products), $perPage, $currentPage, ['path' => $request->url(), 'query' => $request->query()]);

        $view_data = array('pagination' => $pagination);
        return(json_encode($view_data));
    }



    /**
     * @param Request $request
     *  itemSKU : Bar code of the item to add to shipment
     *
     * @return json product record
     */
    public function getProduct(Request $request)
    {
        $data = $request->input();
        $product = Product::where('sku', $data['sku'])
            ->first();

        if ( !$product ) {
            return(['success' => 0, 'msg' => 'Product not found']);
        }

        return(['success' => 1, 'data' => $product]);
    }



    /* API */
    /**
     * @param $id
     * @return array
     */
    public function get($id)
    {

        $shipment = Shipment::with('location', 'products.category.parentCategory', 'products.category.subcategories')
            ->find($id);

        if ( !$shipment ) {
            return(['success' => 0, 'msg' => 'Unable to locate shipment']);
        }

        return(['success' => 1, 'data' => $shipment]);
    }

}
