<?php

namespace App\Http\Controllers;

use App\Http\Requests\GenerateLabelRequest;
use App\Label;
use App\Product;
use Illuminate\Http\Request;

class LabelController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    public function generate( GenerateLabelRequest $request )
    {
        $data = $request->all();

        $productId = $request->get('product_id', null);

        if( $request->has('new_product') ) {
            //Create product
            //TODO: Put this code somewhere else
            $product = Product::create([
                'name' => $data['name'],
                'sku' => $data['sku'],
                'nutrition_label' => $data[''], //TODO: Choose nutrition label from dropdown?
                'key_statement' => $data[''], //TODO: Should be three
                'price' => $data['price'], //Saving price
                'description' => $data['description'],
                'ingredients' => $data['ingredients'],
                'special_instructions' => $data['special_instructions'],
            ]);

            $productId = $product->id;
            //TODO: what to do with ship_date and sell_by

        }

        //Redirect to label generator
        return redirect()->route('show-label', [
            $productId
        ]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $products = Product::get();

        $products = $products->pluck('name', 'id')->all();

        return view('labels.index', compact('products'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Label  $label
     * @return \Illuminate\Http\Response
     */
    public function show(Label $label)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Label  $label
     * @return \Illuminate\Http\Response
     */
    public function edit(Label $label)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Label  $label
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Label $label)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Label  $label
     * @return \Illuminate\Http\Response
     */
    public function destroy(Label $label)
    {
        //
    }
}
