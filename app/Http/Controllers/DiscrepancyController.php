<?php

namespace App\Http\Controllers;

use App\Services\Discrepancies;
use App\Http\Requests\StoreDiscrepancyRequest;

class DiscrepancyController extends Controller
{
    public function getDiscrepancies(StoreDiscrepancyRequest $request, Discrepancies $discrepancies)
    {
        $data = $request->all();
        $products = $data['shipment']['products'];
        $email = $data['shipment']['email'];
        return $discrepancies->get($products, $data['shipment']['id'], $email);
    }
}
