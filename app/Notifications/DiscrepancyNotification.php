<?php

namespace App\Notifications;

use Illuminate\Bus\Queueable;
use Illuminate\Notifications\Notification;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Notifications\Messages\MailMessage;

class DiscrepancyNotification extends Notification
{
    use Queueable;

    protected $discrepancies;

    /**
     * Create a new notification instance.
     *
     * @param $discrepancies
     */
    public function __construct($discrepancies)
    {
        //
        $this->discrepancies = $discrepancies;
    }

    /**
     * Get the notification's delivery channels.
     *
     * @param  mixed  $notifiable
     * @return array
     */
    public function via($notifiable)
    {
        return ['mail'];
    }

    /**
     * Get the mail representation of the notification.
     *
     * @param  mixed  $notifiable
     * @return \Illuminate\Notifications\Messages\MailMessage
     */
    public function toMail($notifiable)
    {
        return (new MailMessage)
            ->subject('Farm Cut Receive Shipment Discrepancy Entered')
            ->cc(env('DISCREPANCY_EMAIL_2', 'elvira.yambot@tonehouse.com'))
//                    ->line('Hello! There was an error when entering inventory. Please login to your Farm Cut account for more details.')
            ->markdown('notifications.discrepancy', [
                'url' => env('APP_URL'),
                'discrepancies' => $this->discrepancies
            ]);
    }

    /**
     * Get the array representation of the notification.
     *
     * @param  mixed  $notifiable
     * @return array
     */
    public function toArray($notifiable)
    {
        return [
            //
        ];
    }
}
