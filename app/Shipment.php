<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use App\Location;
use App\Product;
use Carbon\Carbon;
use Illuminate\Support\Arr;
use OwenIt\Auditing\Contracts\Auditable;


class Shipment extends Model implements Auditable
{
    use SoftDeletes, \OwenIt\Auditing\Auditable;

    protected $fillable = [
        'location_id',
        'status',
        'sku',
    ];


    protected $appends = [
        'total_quantity',
        'location_name',
        'shipped_date',
    ];


    public function location()
    {
        return $this->belongsTo('App\Location');
    }


    public function discrepancies()
    {
        return $this->hasMany(Discrepancy::class);
    }

    public function products()
    {
        return $this->belongsToMany('App\Product', 'product_shipment')->withTrashed()->withPivot('id', 'quantity');
    }

    public function getSkuAttribute()
    {
        if ( $this->getOriginal('sku') == '' ) {
            return sprintf("%08d", $this->attributes['id']);
        }
        return $this->getOriginal('sku');
    }

    public function getTotalQuantityAttribute() {
        return($this->products()->sum('quantity'));
    }

    public function getLocationNameAttribute() {
        return($this->location->name);
    }

    public function getShippedDateAttribute() {
        return(Carbon::parse($this->attributes['created_at'])->toDateString());
    }

    /**
     * {@inheritdoc}
     */
    public function transformAudit(array $data): array
    {
        if (Arr::has($data, 'new_values.sku')) {
            if ($data['new_values']['sku'] === '0') {
                $data['new_values']['order_id'] = $this->getAttribute('sku');
            }
        }

        return $data;
    }

}
