<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use OwenIt\Auditing\Contracts\Auditable;

class Product extends Model implements Auditable
{
    use SoftDeletes, \OwenIt\Auditing\Auditable;

    protected $fillable = [
        'name',
        'category_id',
        'sku',
        'nutrition_label',
        'key_statements',
        'key_benefits',
        'price',
        'description',
        'ingredients',
        'allergens',
        'nutrition_facts',
        'special_instructions',
    ];

    protected $casts = [
        'key_statements' => 'object',
        'key_benefits' => 'object',
    ];

    protected $dates = [
        'created_at',
        'updated_at',
        'deleted_at',
    ];

    public function category()
    {
        return $this->belongsTo('App\Category');
    }


    public function locations()
    {
        return $this->belongsToMany('App\Location', 'location_product')->withPivot('par_quantity');
    }

    public function discrepancies()
    {
        return $this->hasMany(Discrepancy::class);
    }


    public function setNameAttribute( $name )
    {
        $this->attributes['name'] = $name;
    }

    public function setPriceAttribute( $price )
    {
        $this->attributes['price'] = float_to_pennies($price);
    }

    public function getPriceAttribute()
    {
        //return pennies_to_float($this->getAttribute('price')); :X
        return pennies_to_float($this->attributes['price']);
    }
}
