<?php
/**
 * Created by PhpStorm.
 * User: andresabello
 * Date: 3/20/18
 * Time: 1:48 PM
 */

namespace App\Services;


use App\Shipment;
use App\Discrepancy;
use Illuminate\Support\Facades\Notification;
use App\Notifications\DiscrepancyNotification;

class Discrepancies
{
    /**
     * @var Discrepancy
     */
    protected $shipmentId;

    /**
     * @var
     */
    protected $userEmail;

    /**
     * @param $products
     * @param $shipmentId
     * @param $email
     * @return string
     */
    public function get($products, $shipmentId, $email)
    {
        $shipment = Shipment::find($shipmentId);

        if ($shipment) {
            $this->setShipmentId($shipmentId);
            $this->setUserEmail($email);

            $discrepancies = array_filter($products, [$this, 'isDiscrepancy']);
            if (count($discrepancies) > 0) {
                $discrepancies = collect(array_map([$this, 'create'], $discrepancies));
                Notification::route('mail', env('DISCREPANCY_EMAIL'))
                    ->notify(new DiscrepancyNotification($discrepancies));
                return $discrepancies;
            }
        }

        return 'Please enter a valid shipment';
    }

    /**
     * @param $shipmentId
     */
    public function setShipmentId($shipmentId)
    {
        $this->shipmentId = $shipmentId;
    }

    /**
     * @param $email
     */
    public function setUserEmail($email)
    {
        $this->userEmail = $email;
    }

    /**
     * Pass an array with discrepancies discrepancies => [[item], [item]]
     * @param array $discrepancy
     * @return Discrepancy|\Illuminate\Database\Eloquent\Model
     */
    public function create($discrepancy)
    {

        return Discrepancy::create([
            'shipment_id' => $this->shipmentId,
            'product_id' => $discrepancy['id'],
            'shipped' => $discrepancy['shipped'],
            'received' => $discrepancy['received'],
            'user_email' => $this->userEmail,
        ]);
    }

    /**
     * @param array $item
     * @return bool
     */
    protected function isDiscrepancy($item)
    {
        return intval($item['shipped']) !== intval($item['received']);
    }

}