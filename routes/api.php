<?php

use Illuminate\Http\Request;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::middleware('auth:api')->get('/user', function (Request $request) {
    return $request->user();
});

//Route::get('/v1/shipments/get-disc', 'DiscrepancyController@getDiscrepancies');
Route::post('v1/discrepancy', 'DiscrepancyController@getDiscrepancies');
Route::get('v1/discrepancy', function(){
    return 'hello';
});
