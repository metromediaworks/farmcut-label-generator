<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/
Route::group(['middleware'=> 'csrf'], function(){
    Route::auth();

    Route::get('/logout', function () {
        Auth::logout();
        return redirect('login');
    });

    Route::get('/', function () {
        return redirect('login');
    });


    Route::group(['middleware' => 'auth'], function() {

        Route::get('/home', function() {
            return view('home');
        });

//    Route::get('label/generate', 'LabelController@create');
//    Route::post('label/generate/show/{product_id}', 'LabelController@generate')->name('show-label');


        Route::get('product', 'ProductController@index');
        Route::post('product/list', 'ProductController@list');
        Route::post('product/instructions', 'ProductController@instructions');
        Route::post('product/skus', 'ProductController@skus');
        Route::post('product/names', 'ProductController@names');
        Route::get('product/create', 'ProductController@create');
        Route::post('product/delete', 'ProductController@delete');
        Route::post('product/save', 'ProductController@save')->name('product.update');
        Route::post('product/store', 'ProductController@store')->name('product.store');
        Route::get('product/edit/{id}', 'ProductController@edit');
        Route::get('product/categories', 'CategoryController@tree');
        Route::put('product/categories', 'CategoryController@add');
        Route::post('product/categories/delete', 'CategoryController@delete');

        Route::get('shipment', 'ShipmentController@index');
        Route::get('shipment/show/{id?}', 'ShipmentController@show');
        Route::get('shipment/print/{id?}', 'ShipmentController@print');
        Route::get('shipment/download/{shipment}', 'ShipmentController@download');
        Route::post('shipment/list', 'ShipmentController@list');
        Route::post('shipment/locations', 'ShipmentController@getLocations');
        Route::post('shipment/product', 'ShipmentController@getProduct');
        Route::post('shipment/products', 'ShipmentController@getProducts');
        Route::post('shipment/save', 'ShipmentController@save');
        Route::get('shipment/create', 'ShipmentController@create');
        Route::get('shipment/duplicate/{shipment}', 'ShipmentController@duplicate');
        Route::post('shipment/barcode', 'ShipmentController@generateBarcode');
        Route::post('shipment/find', 'ShipmentController@findMatchingShipments');

        Route::post('benefit/list', 'BenefitController@list');

    });

});


/* REST API */
Route::get('API/v1/shipments/{id}', 'ShipmentController@get');
Auth::routes();

Route::get('/home', 'HomeController@index')->name('home');
