<?php

use Faker\Generator as Faker;

$factory->define(App\Product::class, function (Faker $faker) {
    return [
        'name' => $faker->word,
        'category_id' => factory(\App\Category::class)->create()->id,
        'sku' => $faker->isbn10,
        'nutrition_label' => $faker->paragraph,
        'key_statements' => json_encode([null, null, null]),
        'key_benefits' => json_encode([null, null, null]),
        'price' => $faker->numberBetween(1,100),
        'description' => $faker->paragraph,
        'ingredients' => $faker->paragraph,
        'special_instructions' => $faker->paragraph,
    ];
});
