<?php
use Faker\Generator as Faker;

$factory->define(App\Shipment::class, function (Faker $faker) {
    return [
        'location_id' => factory(\App\Location::class)->create()->id,
        'sku' => $faker->isbn10,
        'status' => $faker->boolean(90)
    ];
});
