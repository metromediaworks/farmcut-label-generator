<?php

use Faker\Generator as Faker;

$factory->define(App\Discrepancy::class, function (Faker $faker) {
    return [
        'shipment_id' => factory(\App\Shipment::class)->create()->id,
        'product_id' => factory(\App\Product::class)->create()->id,
        'shipped' => $faker->numberBetween(1,50),
        'received' => function($discrepancy) use($faker){
            return $faker->numberBetween(1, $discrepancy->shipped);
        },
        'user_email' => $faker->email,
    ];
});
