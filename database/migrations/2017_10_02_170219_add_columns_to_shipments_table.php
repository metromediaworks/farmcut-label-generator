<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddColumnsToShipmentsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('shipments', function(Blueprint $table) {
            $table->unsignedInteger('location_id')->after('id');
            $table->string('sku', 100)->after('location_id')->nullable();
            $table->tinyInteger('status')->after('sku')->default(1);
            $table->softDeletes();
        });

        Schema::table('shipments', function(Blueprint $table) {
            $table->foreign('location_id')->references('id')->on('locations')
            ->onDelete('no action')
            ->onUpdate('no action');
        });

    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('shipments', function(Blueprint $table) {
            $table->drop_column('location_id');
            $table->drop_column('status');
            $table->drop_column('sku');
            $table->drop_column('deleted_at');
        });
    }
}
