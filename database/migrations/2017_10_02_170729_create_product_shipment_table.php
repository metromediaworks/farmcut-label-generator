<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateProductShipmentTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {

        Schema::create('product_shipment', function (Blueprint $table) {
            $table->increments('id');
            $table->unsignedInteger('shipment_id');
            $table->unsignedInteger('product_id');
            $table->integer('quantity')->nullable();
            $table->timestamps();
        });

        Schema::table('product_shipment', function(Blueprint $table) {
            $table->foreign('shipment_id')->references('id')->on('shipments')
            ->onDelete('no action')
            ->onUpdate('no action');
            $table->foreign('product_id')->references('id')->on('products')
            ->onDelete('no action')
            ->onUpdate('no action');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('product_shipment');
    }
}
