<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddColumnsToLocationTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('locations', function(Blueprint $table) {
            $table->renameColumn('zip_code', 'zip');
            $table->string('name', 300)->after('id')->nullable();
            $table->string('alt_phone', 15)->after('phone')->nullable();
            $table->string('address2', 100)->after('address')->nullable();

        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('locations', function(Blueprint $table) {
            $table->renameColumn('zip', 'zip_code');
            $table->dropColumn(['name', 'address2', 'alt_phone']);

        });
    }
}
