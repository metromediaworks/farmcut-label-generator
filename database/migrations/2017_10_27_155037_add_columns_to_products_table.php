<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddColumnsToProductsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('products', function(Blueprint $table) {

            $table->string('key_statement', 300)->change();
            $table->string('key_benefits', 90)->after('key_statement')->nullable();
            $table->text('nutrition_facts')->after('key_benefits')->nullable();
            $table->text('allergens')->after('nutrition_facts')->nullable();
            $table->renameColumn('key_statement', 'key_statements');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {

        Schema::table('products', function(Blueprint $table) {
            $table->rename('key_statements', 'key_statement');
            $table->dropColumn('key_benefits');
            $table->dropColumn('nutrition_facts');
            $table->dropColumn('allergens');
        });
    }
}
