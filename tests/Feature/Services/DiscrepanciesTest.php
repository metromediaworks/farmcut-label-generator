<?php
/**
 * Created by PhpStorm.
 * User: andresabello
 * Date: 3/20/18
 * Time: 1:47 PM
 */

namespace Tests\Feature\Services;


use Tests\TestCase;
use App\Discrepancy;
use App\Services\Discrepancies;
use Illuminate\Support\Facades\Notification;
use App\Notifications\DiscrepancyNotification;
use Illuminate\Notifications\AnonymousNotifiable;
use Illuminate\Foundation\Testing\DatabaseTransactions;

class DiscrepanciesTest extends TestCase
{
    use DatabaseTransactions;

    protected $discrepancies;

    public function setUp()
    {
        parent::setUp();
        $this->discrepancies = app()->make(Discrepancies::class);
    }

    /**
     * @test
     */
    public function it_makes_sure_there_is_a_discrepancy_on_the_shipment_based_on_data_received()
    {
        $data = $this->incomingShipmentData();
        $data = json_decode($data, true);
        $discrepancies = $this->discrepancies->get($data['shipment']['products'], $data['shipment']['id'], 'andres.abello@gmail.com');
        $this->assertCount(1, $discrepancies);
    }

    /**
     * @test
     */
    public function it_creates_discrepancies_when_the_received_and_shipped_values_are_not_the_same()
    {
        $data = $this->incomingShipmentData();
        $data = json_decode($data, true);
        $discrepancies = $this->discrepancies->get($data['shipment']['products'], $data['shipment']['id'], 'andres.abello@gmail.com');
        $this->assertInstanceOf(Discrepancy::class, $discrepancies->first());
    }

    /**
     * @test
     */
    public function it_send_an_email_reporting_discrepancies_when_there_are_discrepancies()
    {
        Notification::fake();
        $data = $this->incomingShipmentData();
        $data = json_decode($data, true);
        $this->discrepancies->get($data['shipment']['products'], $data['shipment']['id'], 'andres.abello@gmail.com');
        Notification::assertSentTo(
            new AnonymousNotifiable(),
            DiscrepancyNotification::class,
            function ($notification, $channels, $notifiable) {
                return $notifiable->routes['mail'] == 'andres.abello@agileelement.com';
            }
        );
    }

    /**
     * @test
     */
    public function it_integrates_the_full_suite()
    {
        $data = json_decode($this->incomingShipmentDataSecond(), true);
        $data['shipment']['email'] = 'andres.abello@agileement.com';
        $response = $this->json('POST', '/api/v1/discrepancy', $data);
        $response->assertStatus(200);
    }

    protected function incomingShipmentData()
    {
        return '{"shipment":{"id":84,"location_id":1,"sku":"00000084","status":1,"created_at":"2018-02-20 20:21:02","updated_at":"2018-02-20 20:21:02","deleted_at":null,"total_quantity":"2","location_name":"TH-NOMAD","shipped_date":"2018-02-20","location":{"id":1,"name":"TH-NOMAD","address":"32 E 31st St","address2":null,"phone":null,"alt_phone":null,"city":"New York","state":"NY","zip":"10016","brand_id":1,"created_at":"2017-08-23 13:58:16","updated_at":"2017-08-23 13:58:17","full_address":"32 E 31st St , New York, NY 10016"},"products":[{"id":67,"name":"Banana Protein Muffins","category_id":1,"sku":"123456789","nutrition_label":null,"key_statements":[null,null,null],"key_benefits":[0,0,0,0,0],"nutrition_facts":null,"allergens":null,"price":5.99,"description":null,"ingredients":null,"special_instructions":null,"created_at":"2018-02-20 16:08:09","updated_at":"2018-02-22 14:50:49","deleted_at":"2018-02-22 14:50:49","pivot":{"shipment_id":84,"product_id":67,"id":204,"quantity":1},"category":{"id":1,"name":"Breakfast","parent_id":4,"created_at":"2018-02-16 15:57:22","updated_at":"2018-02-16 15:57:22","deleted_at":null,"parent_category":{"id":4,"name":"Nutrition","parent_id":null,"created_at":null,"updated_at":null,"deleted_at":null}},"received":"1","shipped":1},{"id":62,"name":"Test Breakfast2","category_id":1,"sku":"1990","nutrition_label":null,"key_statements":[null,null,null],"key_benefits":[0,0,0,0,0],"nutrition_facts":null,"allergens":null,"price":8,"description":null,"ingredients":null,"special_instructions":null,"created_at":"2018-02-20 15:55:06","updated_at":"2018-02-22 14:27:18","deleted_at":"2018-02-22 14:27:18","pivot":{"shipment_id":84,"product_id":62,"id":205,"quantity":1},"category":{"id":1,"name":"Breakfast","parent_id":4,"created_at":"2018-02-16 15:57:22","updated_at":"2018-02-16 15:57:22","deleted_at":null,"parent_category":{"id":4,"name":"Nutrition","parent_id":null,"created_at":null,"updated_at":null,"deleted_at":null}},"received":"1","shipped":2}]},"location":"2"}';
    }

    protected function incomingShipmentDataSecond()
    {
        return '{"shipment":{"id":169,"location_id":1,"sku":"00000169","status":1,"created_at":"2018-03-15 12:48:46","updated_at":"2018-03-15 12:48:46","deleted_at":null,"total_quantity":"2","location_name":"TH-NOMAD","shipped_date":"2018-03-15","location":{"id":1,"name":"TH-NOMAD","address":"32 E 31st St","address2":null,"phone":null,"alt_phone":null,"city":"New York","state":"NY","zip":"10016","brand_id":1,"created_at":"2017-08-23 13:58:16","updated_at":"2017-08-23 13:58:17","full_address":"32 E 31st St , New York, NY 10016"},"products":[{"id":150,"name":"Chicken Salata","category_id":41,"sku":"3123123","nutrition_label":null,"key_statements":[null,null,null],"key_benefits":[0,0,0,0,0],"nutrition_facts":null,"allergens":null,"price":0,"description":null,"ingredients":null,"special_instructions":null,"created_at":"2018-03-09 21:58:48","updated_at":"2018-03-09 21:58:48","deleted_at":null,"pivot":{"shipment_id":169,"product_id":150,"id":384,"quantity":2},"category":{"id":41,"name":"Seafood","parent_id":40,"created_at":"2018-03-01 19:40:56","updated_at":"2018-03-01 19:40:56","deleted_at":null,"parent_category":{"id":40,"name":"Non-Veg","parent_id":null,"created_at":"2018-03-01 19:40:33","updated_at":"2018-03-01 19:40:33","deleted_at":null},"subcategories":[]},"received":"1","shipped":2}]},"location":"2"}';
    }

}